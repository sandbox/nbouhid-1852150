<?php

/**
 * Obtains the list of modules
 * 
 * @return array A name indexed array with the path of the modules
 */
function _modules_report_obtain_modules() {
  $query = db_select('system', 's')
              ->fields('s', array('filename', 'name'))
              ->condition('type', 'module')
              //->condition('filename', 'sites/all/modules%', 'NOT LIKE')
              //->condition('filename', 'modules/%', 'NOT LIKE')
              ->execute();

  $modules = array();
  while($result = $query->fetchAssoc()) {
    $modules[$result['name']] = $result['filename'];
  }
  
  return $modules;
}